﻿using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Tls;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECC_521
{
    /// <summary>
    /// Wrapper class for KeyValuePair
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public class KeyGenerator<TKey>
    {
        //initialize a list to store the keys
        private static List<TKey> keys = new List<TKey>(); 



        //--------------------------------------------------------------------------------------------------



        /// <summary>
        /// Method to Generate a Public and Private Key Pair
        /// </summary>
        /// <param name="domainParameters"></param>
        /// <param name="publicKey"></param>
        /// <param name="privateKey"></param>
        private static void GenerateKeyPair(ECDomainParameters domainParameters, out byte[] publicKeyEncoded, out byte[] privateKeyEncoded, out TimeSpan elapsedPublicKeyTime, out TimeSpan elapsedPrivateKeyTime)
        {
            //define a stopwatch to measure the time it takes to generate the key pair
            var stopwatch = new System.Diagnostics.Stopwatch();

            //define a key pair generator for the curve
            var keyPairGenerator = new ECKeyPairGenerator();

            //define a secure random number generator
            var secureRandom = new SecureRandom();

            //initialize the key pair generator with the domain parameters and secure random number generator
            keyPairGenerator.Init(new ECKeyGenerationParameters(domainParameters, secureRandom));

            //start the stopwatch
            stopwatch.Start();

            //cast the public key to a ECPublicKeyParameters object
            var publicKey = keyPairGenerator.GenerateKeyPair().Public;

            //stop the stopwatch
            stopwatch.Stop();

            //Encode the public key
            publicKeyEncoded = ((ECPublicKeyParameters)publicKey).Q.GetEncoded();

            //return the total time taken to generate the public key
            elapsedPublicKeyTime = stopwatch.Elapsed;

            //Restart the stopwatch
            stopwatch.Restart();

            //cast the private key to a ECPrivateKeyParameters object
            var privateKey = keyPairGenerator.GenerateKeyPair().Private;

            //stop the stopwatch
            stopwatch.Stop();

            //Encode the private key
            privateKeyEncoded = ((ECPrivateKeyParameters)privateKey).D.ToByteArray();

            //return the total time taken to generate the private key
            elapsedPrivateKeyTime = stopwatch.Elapsed;
        }
    }
}
//---------------....oooOO0_END_OF_FILE_0OOooo....---------------\\