﻿using Org.BouncyCastle.Asn1.Nist;
using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math.EC;
using Org.BouncyCastle.Security;
using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using static System.Console;

namespace ECC_521
{
    internal class Program
    {
        //identify NIST curve to use
        private const string curveName = "P-521";
        //initialize the number of keys to generate
        private static int numberOfKeys = 0;
        //initialize performance counters for CPU and RAM
        private static PerformanceCounter cpuCounter = null;
        private static PerformanceCounter ramCounter = null;
        private static Process p = Process.GetCurrentProcess();



        //--------------------------------------------------------------------------------------------------



        /// <summary>
        /// Entry Point For Application
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //run the application
            run();
        }



        //--------------------------------------------------------------------------------------------------



        /// <summary>
        /// Method to Run the Application
        /// </summary>
        private static void run()
        {
            //initialize performance counters
            InitializeCounters();

            //catch running errors
            try
            {
                //define variables for the curve
                ECDomainParameters eCurve = null;
                //define variables for both keys
                byte[] publicKey;
                byte[] privateKey;
                //define variables for time elapsed
                TimeSpan elapasedTimeForCurveDefinition = TimeSpan.Zero;
                TimeSpan elapasedTimeForPublicKeyGeneration = TimeSpan.Zero;
                TimeSpan elapasedTimeForPrivateKeyGeneration = TimeSpan.Zero;

                //get the number of keys to generate
                GetNumberOfKeysToGenerate();

                //display the curve type
                WriteLine("Elliptical curve type: \t" + curveName +  "\n\n");

                //define an elliptical curve (cached to reduce performance bottlenecks)
                DefineCurve(out eCurve, out elapasedTimeForCurveDefinition);

                //set processing time spent to the time spent defining the curve
                TimeSpan totalElapsedTime = elapasedTimeForCurveDefinition;

                //display the time taken to define the curve
                WriteLine("Time taken to define curve: \t\t\t" + elapasedTimeForCurveDefinition.TotalMilliseconds + " milliseconds\n\n");

                //main program loop to generate keys
                for (int i = 0; i < numberOfKeys; i++)
                {
                    //display the key number
                    WriteLine("Key pair number \t" + (i+1) +  ":\n\n");

                    //generate a public and private key pair
                    GenerateKeyPair(eCurve, out publicKey, out privateKey, out elapasedTimeForPublicKeyGeneration, out elapasedTimeForPrivateKeyGeneration);

                    //fetch current cpu usage as a percentage
                    var x = cpuCounter.NextValue();

                    //collect garbage and wait for finalizers to finish
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();

                    //wait for 0.2 second (stabilize the performance counters)
                    Thread.Sleep(500);

                    //get the current CPU usage after processing
                    var cpuUsage = cpuCounter.NextValue();

                    //tally the time spent generating key pairs
                    totalElapsedTime += elapasedTimeForPublicKeyGeneration + elapasedTimeForPrivateKeyGeneration;

                    //display the current CPU usage after processing
                    var formattedStringCPU = String.Format("{0:##0} %", cpuUsage);

                    WriteLine("CPU Usage: \t~" + formattedStringCPU);
                    //display keys generated
                    DisplayOutput(publicKey, privateKey, totalElapsedTime, elapasedTimeForPrivateKeyGeneration, elapasedTimeForPublicKeyGeneration, elapasedTimeForCurveDefinition);  
                }

                //display total time taken to define the curve and generate the key pair
                WriteLine("\n\nTotal time taken: \t" + totalElapsedTime.TotalMilliseconds + " milliseconds//" + totalElapsedTime.TotalSeconds + " seconds\n\n");
                p.Dispose();
            }
            catch (Exception e)
            {
                //display error output
                WriteLine("Error: {0}", e.Message);
            }
        }



        //--------------------------------------------------------------------------------------------------



        /// <summary>
        /// Method to Get the Number of Keys to Generate
        /// </summary>
        private static void GetNumberOfKeysToGenerate()
        {
            WriteLine("Enter the number of keys to generate: ");
            numberOfKeys = Convert.ToInt32(ReadLine());
            Clear();
        }



        //--------------------------------------------------------------------------------------------------



        /// <summary>
        /// Method to Initialize Performance Counters
        /// </summary>
        private static void InitializeCounters()
        {
            //catch running errors
            try
            {
                //initialize performance counters for CPU and RAM
                cpuCounter = new PerformanceCounter
                    ("Processor", "% Processor Time", "_Total");
                ramCounter = new PerformanceCounter
                    ("Memory", "Available MBytes", String.Empty);
            }
            catch (Exception e)
            {
                //display error output
                WriteLine("Error: {0}", e.Message);
            }
        }



        //--------------------------------------------------------------------------------------------------



        /// <summary>
        /// Mehtod to Define an Eliptical Curve
        /// </summary>
        private static void DefineCurve(out ECDomainParameters domainParameters, out TimeSpan elapsedTime)
        {
            //define a stopwatch to measure the time it takes to define the curve
            var stopwatch = new System.Diagnostics.Stopwatch();

            //start the stopwatch
            stopwatch.Start();

            //generate a curve's paramters based on NIST curve selected
            X9ECParameters ecParameters = NistNamedCurves.GetByName(curveName);

            //stop the stopwatch
            stopwatch.Stop();
            
            //cast the curve parameters to a general ECurve object and then cast to a FPCurve for a curve over a prime field
            var generatedCurve = (FpCurve)ecParameters.Curve;

            //generate an eCDomainParameters object to encapsulate the curve's domain parameters of CURVE, BASE POINT, ORDER, COFACTOR, and SEED VALUES
            domainParameters = new ECDomainParameters(generatedCurve, ecParameters.G, ecParameters.N, ecParameters.H, ecParameters.GetSeed());

            //return the total time taken to generate the curve
            elapsedTime = stopwatch.Elapsed;
        }



        //--------------------------------------------------------------------------------------------------



        /// <summary>
        /// Method to Generate a Public and Private Key Pair
        /// </summary>
        /// <param name="domainParameters"></param>
        /// <param name="publicKey"></param>
        /// <param name="privateKey"></param>
        private static void GenerateKeyPair(ECDomainParameters domainParameters, out byte[] publicKeyEncoded, out byte[] privateKeyEncoded, out TimeSpan elapsedPublicKeyTime, out TimeSpan elapsedPrivateKeyTime)
        {
            //define a stopwatch to measure the time it takes to generate the key pair
            var stopwatch = new System.Diagnostics.Stopwatch();

            //define a key pair generator for the curve
            var keyPairGenerator = new ECKeyPairGenerator();

            //define a secure random number generator
            var secureRandom = new SecureRandom();

            //initialize the key pair generator with the domain parameters and secure random number generator
            keyPairGenerator.Init(new ECKeyGenerationParameters(domainParameters, secureRandom));

            //start the stopwatch
            stopwatch.Start();

            //cast the public key to a ECPublicKeyParameters object
            var publicKey = keyPairGenerator.GenerateKeyPair().Public;

            //stop the stopwatch
            stopwatch.Stop();

            //Encode the public key
            publicKeyEncoded = ((ECPublicKeyParameters)publicKey).Q.GetEncoded();

            //return the total time taken to generate the public key
            elapsedPublicKeyTime = stopwatch.Elapsed;

            //Restart the stopwatch
            stopwatch.Restart();

            //cast the private key to a ECPrivateKeyParameters object
            var privateKey = keyPairGenerator.GenerateKeyPair().Private;

            //stop the stopwatch
            stopwatch.Stop();

            //Encode the private key
            privateKeyEncoded = ((ECPrivateKeyParameters)privateKey).D.ToByteArray();

            //return the total time taken to generate the private key
            elapsedPrivateKeyTime = stopwatch.Elapsed;
        }



        //--------------------------------------------------------------------------------------------------


        /// <summary>
        /// Method to Display the Public and Private Key
        /// </summary>
        /// <param name="publicKey"></param>
        /// <param name="privateKey"></param>
        private static void DisplayOutput(byte[] publicKey, byte[] privateKey, TimeSpan totalTimeElapsed, TimeSpan elapsedTimeForPublicKey, TimeSpan elapsedTimeForPrivateKey, TimeSpan elapsedTimeForCurve)
        {
            var output = new StringBuilder();
            output.AppendLine($"Public key: \t\t{publicKey.Length} bytes \t{elapsedTimeForPublicKey.TotalMilliseconds} milliseconds");
            output.AppendLine($"Private key: \t\t{privateKey.Length} bytes \t{elapsedTimeForPrivateKey.TotalMilliseconds} milliseconds\n");
            output.AppendLine($"Public key (first 50 bytes): \t{Convert.ToBase64String(publicKey).Substring(0, Math.Min(publicKey.Length, 50))}");
            output.AppendLine($"Private key (first 50 bytes): \t{Convert.ToBase64String(privateKey).Substring(0, Math.Min(privateKey.Length, 50))}");
            output.AppendLine("==============================================================================");

            WriteLine(output.ToString());
        }
    }
}
//---------------....oooOO0_END_OF_FILE_0OOooo....---------------\\